from xmlrpc.client import Boolean
import joblib
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import constants
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt


# in python by default we use snake_case syntax 

def load_dataset(file_path:str):
    """step 2 - Load Dataset"""
    try:
        return pd.read_csv(file_path)
    except Exception as exp:
        raise exp

# in each function we use the try except block syntax
    # try:
    #     ... your-code-here...
    # except Exception as exp:
    #     raise exp
# if everything goes well the try block is executed
# if something happens the exception explaining the error is raised


def transform_properties(dataset):
    """step 3 - Transform US properties into EU"""
    try:
        dataset['Weight'] = np.around(dataset['Weight'] * constants.pound, 1)
        dataset['Height'] = np.around(dataset['Height'] * constants.inch * 100)
        dataset['Height'] = dataset['Height'].astype(np.int64, errors='ignore') 
        return dataset
    except Exception as exp:
        raise exp



def check_dataset_is_balanced(dataset):
    """step 4 - Check if Genders is Balanced"""
    try:
        pass
        # print(dataset['Gender'].value_counts())
    except Exception as exp:
        raise exp



def plot_dataset(show:Boolean, x:str, y:str, hue:str, dataset):
    """step 5 - Plot Genders depending on Weight and Height"""
    try:
        sns.scatterplot(x=x, y=y, data=dataset, hue=hue)
        if show: 
            plt.show()
    except Exception as exp:
        raise exp



def replace_string_by_int(male:dict, female:dict, dataset):
    """step 6 - Replace string gender Male by Int:0 & Female by Int:1"""
    try:
        dataset.Gender = dataset.Gender.map({**male, **female})
        return dataset
    except Exception as exp:
        raise exp



def train_model_with_linear_regression(dataset):
    """step 7 - Train model with Machine Learning Patterns"""
    try:
        X = dataset[ ["Gender", "Height"] ] # definir input sur l'axe X 
        y = dataset[ ["Weight"] ] # definir output sur l'axe Y
        # Trouver des correlations entre X et Y afin de definir des modeles
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
        # Entrainer le dataset avec un modele de regression lineaire.
        lin_reg = LinearRegression()
        lin_reg.fit(X_train, y_train)
        # print(lin_reg.score(X_test, y_test))
        return lin_reg
    except Exception as exp:
        raise exp



def query_model(lin_reg, genre:int, height:int):
    """step 8 - After the model is trained we can query"""
    try:
        # Donne moi le poids moyen d'une female de 1.80M
        test = np.round(lin_reg.predict([[0, 180]])[0][0],1)
        # Print le poids retournee
        print(f'{genre} {height} = {test}')
        return test
    except Exception as exp:
        raise exp



def save_model(lin_reg):
    """step 9 - Save the trained modele that can be used later"""
    try:
        joblib_file = "WeightPredictionLinRegModel.joblib"
        joblib.dump(lin_reg, joblib_file)
    except Exception as exp:
        raise exp


# after having defined the function
# we can use the next block with 
# if __name__ == "__main__":
# this means that if you just use this single python file with the command `py ml_.py
# it will execute the code inside

if __name__ == "__main__":
    try:
        dataset = load_dataset("weight-height.csv")
        dataset = transform_properties(dataset)
        check_dataset_is_balanced(dataset)
        plot_dataset(show=1, x='Height', y='Weight', hue='Gender', dataset=dataset)
        dataset = replace_string_by_int({'Male':0}, {'Female':1}, dataset)
        lin_reg = train_model_with_linear_regression(dataset)
        query_model(lin_reg, 0, 180)
        save_model(lin_reg)
    except Exception as exp:
        raise exp

